# source profile
source $HOME/.profile
eval "$(oh-my-posh init zsh --config https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/zash.omp.json)"

# source fzf
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
[ -f ~/.forgit/forgit.plugin.zsh ] && source ~/.forgit/forgit.plugin.zsh

# source docker-machine completion
fpath=(~/.zsh/completion $fpath)

[[ "$TERM_PROGRAM" == "vscode" ]] && . "$(code --locate-shell-integration-path zsh)"


# JINA_CLI_BEGIN

## autocomplete
if [[ ! -o interactive ]]; then
    return
fi

compctl -K _jina jina

_jina() {
  local words completions
  read -cA words

  if [ "${#words}" -eq 2 ]; then
    completions="$(jina commands)"
  else
    completions="$(jina completions ${words[2,-2]})"
  fi

  reply=(${(ps:
:)completions})
}

# session-wise fix
ulimit -n 4096
export OBJC_DISABLE_INITIALIZE_FORK_SAFETY=YES

fzf-history-widget() {
  local selected_history=$(fc -ln -200 | fzf +s --tac +m)
  if [[ -n $selected_history ]]; then
    LBUFFER="${selected_history}"
  fi
}

zle -N fzf-history-widget
bindkey '^R' fzf-history-widget 

# JINA_CLI_END
















if [ -f "/Users/monotykamary/.config/fabric/fabric-bootstrap.inc" ]; then . "/Users/monotykamary/.config/fabric/fabric-bootstrap.inc"; fi