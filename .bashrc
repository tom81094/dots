# .bashrc

set -h

# Source global definitions
if [ -f /etc/skel/.bashrc ]; then
    . /etc/skel/.bashrc
fi

function prev() {
    PREV=$(echo `history | tail -n2 | head -n1` | sed 's/[0-9]* //')
    sh -c "pet new `printf %q "$PREV"`"
}

function pet-select() {
    BUFFER=$(pet search --query "$READLINE_LINE")
    READLINE_LINE=$BUFFER
    READLINE_POINT=${#BUFFER}
}

if [[ $- == *i* ]]; then
    bind -x '"\C-x\C-r": pet-select'
fi

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions
#export TERM=xterm-256color
source $HOME/.profile
source <(devbox completion bash)

# export HISTCONTROL=ignoredups:erasedups
# export HISTSIZE=-1
# export HISTFILESIZE=-1
# shopt -s histappend
# export PROMPT_COMMAND="${PROMPT_COMMAND:+$PROMPT_COMMAND$'\n'}history -a; history -c; history -r"

[ -f ~/.fzf.bash ] && . ~/.fzf.bash || true
[ -f ~/.forgit/forgit.plugin.sh ] && . ~/.forgit/forgit.plugin.sh || true

printf '\eP$f{"hook": "SourcedRcFileForWarp", "value": { "shell": "bash"}}\x9c'



# JINA_CLI_BEGIN

## autocomplete
_jina() {
  COMPREPLY=()
  local word="${COMP_WORDS[COMP_CWORD]}"

  if [ "$COMP_CWORD" -eq 1 ]; then
    COMPREPLY=( $(compgen -W "$(jina commands)" -- "$word") )
  else
    local words=("${COMP_WORDS[@]}")
    unset words[0]
    unset words[$COMP_CWORD]
    local completions=$(jina completions "${words[@]}")
    COMPREPLY=( $(compgen -W "$completions" -- "$word") )
  fi
}

complete -F _jina jina

# session-wise fix
ulimit -n 4096
export OBJC_DISABLE_INITIALIZE_FORK_SAFETY=YES
# default workspace for Executors

# JINA_CLI_END
















if [ -f "/Users/monotykamary/.config/fabric/fabric-bootstrap.inc" ]; then . "/Users/monotykamary/.config/fabric/fabric-bootstrap.inc"; fi